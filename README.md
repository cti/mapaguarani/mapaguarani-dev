# Mapa Guarany Yvyrupá

# Projeto Django Base

Projeto Django baseado no padrão [cookiecutter-django](http://cookiecutter-django.readthedocs.io/en/latest) e com diversas adaptações para uso na infraestrutura Docker-Rancher do hacklab/.

## Ambiente de desenvolvimento
Para iniciar o ambiente de desenvolvimento :
```docker-compose up```

Em alguns casos precisamos rever as permissões dos arquivos node_modules, package-lock e pasta staticfiles
 
## Importanto Bando de dados de testes (ou da produção)

Neste exemplo, apagamos e recriamos o banco para garantir que estamos importando sobre uma base limpa.
Este exemplo cobre a importação feita a partir de um binário (psqlc), adapte o os comandos para usar com outros formatos.

```
# Sobe somente container do postgres, importantes senão o django vai bloquer alterações no banco
docker-compose up postgres
# copia backup para dentro do container (isso pode ser melhorado)
docker cp mapaguarani.psqlc mapaguarani_postgres_1:/mapaguarani.psqlc
docker-compose exec postgres sh -c "dropdb -U\$POSTGRES_USER django"
docker-compose exec postgres sh -c "createdb -U\$POSTGRES_USER django"
docker-compose exec postgres sh -c "psql -U\$POSTGRES_USER -d django -c \"CREATE EXTENSION postgis;\""
docker-compose exec postgres sh -c "pg_restore -U\$POSTGRES_USER -O -x -n public -d django mapaguarani.psqlc"
```

Caso o dump seja em texto (arquivo .sql), acesse o container do postgres e faça o restore. 

> Nota: antes de fazer o restore, coloque uma linha `tail -f /dev/null` antes do `run_server` no arquivo 
`compose/local/django/runserver.sh` e execute o docker-compose up --build -d de novo.


```sh
docker cp dump.sql mapaguarani-dev-postgres-1:/tmp/dump.sql
# dentro do container do postgres
psql -U $POSTGRES_USER

# Dentro do postgres
\c postgres
drop database django;
ALTER DATABASE template1 REFRESH COLLATION VERSION;
create database django;
\q

# Fora do postgres mas dentro do container
psql -U $POSTGRES_USER < /tmp/dump.sql
```

Depois de rodar isso, pode tirar o `tail -f /dev/null` e rodar o docker-compose up de novo.

## Testes

Existem duas maneiras de se executar os testes automatizados localmente:

- Você já executou o comando `docker-compose up` e o servidor está funcionando.

```
docker-compose -f local.yml exec django pytest
```

- Você deseja apenas executar os testes sem necessariamente levantar o servidor. Antes é necessário construir a imagem do backend e disponibilizar o banco de dados para então executar o pytest via `docker run`

```
docker build -f compose/test/django/Dockerfile -t django_test .
docker run -d --env-file=./compose/test/test_env --name=postgres_test postgres:9.6
docker run --env-file=./compose/test/test_env --link=postgres_test:postgres \
  django_test /test.sh
```

## Produção

Buildando as imagens de prod:

```sh
docker buildx build --platform linux/amd64 --target django -t us-east1-docker.pkg.dev/${GCP_PROJECT}/mg-gcr/mapaguarani-django:0.1.0 -f compose/production/Dockerfile .
docker buildx build --platform linux/amd64 --target nginx -t us-east1-docker.pkg.dev/${GCP_PROJECT}/mg-gcr/mapaguarani-nginx:0.1.0 -f compose/production/Dockerfile . 
```

## Variáveis de ambiente
### Banco de dados
- POSTGRES_HOST - opcional; padrão 'postgres'
- POSTGRES_DB - obrigatório
- POSTGRES_USER - obrigatório
- POSTGRES_PASSWORD - obrigatório

### Email
- MAILGUN_SENDER_DOMAIN - obrigatório em produção
- DJANGO_DEFAULT_FROM_EMAIL - obrigatório em produção
- DJANGO_MAILGUN_API_KEY - obrigatório em produção

### Django
- DJANGO_ALLOWED_HOSTS - obrigatório em produção
- DJANGO_ADMIN_URL - opcional
- DJANGO_SETTINGS_MODULE - opcional; use `config.settings.production` em produção
- DJANGO_ACCOUNT_ALLOW_REGISTRATION - opcional; padrão True
- DJANGO_SECRET_KEY - obrigatório em produção
- USE_CACHE - opcional; padrão True
- USE_DOCKER - opcional; desnecessário em produção; em ambientes locais, escreva 'yes' se estiver usando Docker

### Redis
- REDIS_URL - obrigatório em produção; exemplo: `redis://127.0.0.1:6379`

### Sentry
- DJANGO_SENTRY_DSN - opcional; só válido em produção

## Integrações de deploy
**Commits no branch `master`** fazem releases da versão em **desenvolvimento**.

**Tags** fazem releases em [**produção**](http://guarani.map.as/).

## Como reiniciar o serviço na Linode em caso de falha

Dado o problema de que pode ocorrer um problema com alto processamento de CPU e a máquina ficar lenta ou até travar esse processo vai ajudar como resolver. Problema "Soft lockup, CPU stuck"

1. Acessar Linode via painel administrativo > ir em Linodes > escolher o "application" 

2. Clicar em "Reboot" e aguardar

Após reiniciar a máquina vai ficar como "Running" e precisamos executar alguns comando no terminal para levantar a aplicação novamente.

1. Acesse a máquina de gerenciamento `ssh root@$IP` (pegar IP no painel administrativo)

2. Acesse a máquina de aplicação `ssh cti.app`

3. Reinicie o redis com o comando `podman container restart redis`

4. Reinicie o django com o comando `podman container restart django`

5. Reiniciei o nginx com o comando `podman container restart nginx`

# Dados Extras de Mapa

Os dados de países e estados de Paraguai, Bolivia e Argentina vieram do Who's On First Gazeteer.

Os dados de limites municipais da Argentina vêm do portal de dados abertos (https://datos-ignarg.opendata.arcgis.com/datasets/3f2410c818c747b3840e6934b45c3daa_13)
