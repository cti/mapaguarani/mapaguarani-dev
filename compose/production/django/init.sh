#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

python /app/manage.py collectstatic --noinput
python /app/manage.py compress
python /app/manage.py compilemessages