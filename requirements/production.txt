# Pro-tip: Try not to put anything here. There should be no dependency in
#	production that isn't in development.
-r base.txt

# WSGI Handler
# ------------------------------------------------
gevent==23.9.1
gunicorn==21.2.0

# Email backends for Mailgun, Postmark, SendGrid and more
# -------------------------------------------------------
django-anymail==10.2

# Raven is the Sentry client
# --------------------------
raven==6.5.0

Werkzeug==2.0.3
django-extensions==3.2.3
