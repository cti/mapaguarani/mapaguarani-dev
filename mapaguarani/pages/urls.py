from rest_framework.routers import SimpleRouter
from . import views


router = SimpleRouter()
router.register(r'', views.FlatpageViewSet, basename='flatpage')

urlpatterns = []

urlpatterns.extend(router.urls)
