from django.urls import include, path, re_path
from django.contrib import admin
from django.views.generic.base import TemplateView
from core.models import CtiProtectedArea, CtiCity, CtiState, CtiCountry
from rest_framework import routers
from rest_framework_cache.registry import cache_registry
from spillway import urls
from django.conf import settings
from core.views import StateByCountryView, CityByStatesView

# Autodiscover for admin and cache registry
admin.autodiscover()
cache_registry.autodiscover()

from core.views import (
    IndigenousLandViewSet, IndigenousVillageViewSet, ArchaeologicalPlaceViewSet, 
    LandTenureViewSet, LandTenureStatusViewSet, IndigenousLandsShapefileView, 
    IndigenousVillagesShapefileView, ArchaeologicalPlaceKMLView, EthnicGroupViewSet, 
    ArchaeologicalPlacesShapefileView, ArchaeologicalPlaceExportView, LandTenureReportViewSet,
    IndigenousVillageGeojsonView, SimpleIndigenousVillageViewSetWithPosition, 
    ArchaeologicalPlaceGeojsonView, IndigenousVillageExportView, IndigenousVillageKMLView, 
    IndigenousLandExportView, IndigenousLandKMLView, ProjectsViewSet, ReportView, 
    EmbeddableTemplateView, ProtobufTileView, LandsProtobufTileView, ProminentEthnicSubGroupViewSet
)

# Creating the router
router = routers.SimpleRouter()
try:
    router.register(r'ethnic_groups', EthnicGroupViewSet, basename='ethnic_groups')
    router.register(r'prominent_ethnic_subgroups', ProminentEthnicSubGroupViewSet, basename='prominent_ethnic_subgroups')
    router.register(r'lands', IndigenousLandViewSet, basename='lands')
    router.register(r'land_tenures', LandTenureViewSet, basename='land_tenures')
    router.register(r'land_tenures_status', LandTenureStatusViewSet, basename='land_tenures_status')
    router.register(r'villages', IndigenousVillageViewSet, basename='indigenousvillage')
    router.register(r'cached_villages', IndigenousVillageViewSet, basename='cached-indigenousvillage')
    router.register(r'simple_villages_with_position', SimpleIndigenousVillageViewSetWithPosition,  basename='simpleindigenousvillage')
    router.register(r'villages_geojson', IndigenousVillageGeojsonView,basename='indigenousvillagegeojsonview' )
    router.register(r'lands_kml', IndigenousLandKMLView, basename='lands-kml')
    router.register(r'villages_kml', IndigenousVillageKMLView, basename='villages-kml')
    router.register(r'arch_kml', ArchaeologicalPlaceKMLView, basename='arch-kml')
    router.register(r'arch_geojson', ArchaeologicalPlaceGeojsonView, basename='arch_geojson')
    router.register(r'archaeological', ArchaeologicalPlaceViewSet, basename='archaeological')
    router.register(r'landtenurereport', LandTenureReportViewSet, basename='landtenurereport')
    router.register(r'projects', ProjectsViewSet, basename='projects')
except ImportError as e:
    print(f"Erro ao importar as views: {e}")
    pass

urlpatterns = [
    path('get_states/<int:country_id>/', StateByCountryView.as_view(), name='get_states_by_country'),
    path('get_cities/', CityByStatesView.as_view(), name='get_cities_by_states'),    
    path('', TemplateView.as_view(template_name='index.html'), name='home'),
    path('embed/', EmbeddableTemplateView.as_view(template_name='embed.html'), name='embed'),
    
    # API routes
    path('api/', include(router.urls)),
    path('api/lands_report', ReportView.as_view(), name='report_view'),
    path('api/pages/', include(('pages.urls', 'pages'), namespace='pages')),
    
    # Export routes
    path('export/villages.xlsx', IndigenousVillageExportView.as_view(), name='export_xls_villages'),
    path('export/lands.xlsx', IndigenousLandExportView.as_view(), name='export_xls_lands'),
    path('export/archaeological.xlsx', ArchaeologicalPlaceExportView.as_view(), name='export_xls_archaeological'),
    
    # Shapefiles routes
    path('shapefiles/villages/', IndigenousVillagesShapefileView.as_view(), name='villages_shapefiles'),
    path('shapefiles/lands/', IndigenousLandsShapefileView.as_view(), name='lands_shapefiles'),
    path('shapefiles/archaeological/', ArchaeologicalPlacesShapefileView.as_view(), name='archaeological_shapefiles'),
    
    # Admin and other external apps
    path('admin/', admin.site.urls),
    path('accounts/', include('allauth.urls')),
    path('rosetta/', include('rosetta.urls')),
    path('i18n/', include('django.conf.urls.i18n')),
    
    # Static JS file
    path('djangular.js', TemplateView.as_view(template_name='djangular.js', content_type='text/javascript'), name='djangular'),
    
    # Tile routes
    re_path(urls.tilepath(r'^tiles/lands/'), LandsProtobufTileView.as_view(), name='lands-tile'),
    re_path(urls.tilepath(r'^tiles/protected_areas/'), 
            ProtobufTileView.as_view(queryset=CtiProtectedArea.objects.all(), layer='lands'), 
            name='protected-areas-tile'),
    re_path(urls.tilepath(r'^tiles/cities/'), 
            ProtobufTileView.as_view(queryset=CtiCity.objects.all(), layer='boundaries'), 
            name='cities-tile'),
    re_path(urls.tilepath(r'^tiles/states/'), 
            ProtobufTileView.as_view(queryset=CtiState.objects.all(), layer='boundaries'), 
            name='states-tile'),
    re_path(urls.tilepath(r'^tiles/countries/'), 
            ProtobufTileView.as_view(queryset=CtiCountry.objects.all(), layer='boundaries'), 
            name='countries-tile'),
]

if settings.DEBUG:
    try:
        import debug_toolbar
        urlpatterns = [
            path('__debug__/', include(debug_toolbar.urls)),
        ] + urlpatterns
    except ImportError as e:
        print(f"Erro ao importar as views: {e}")
        pass
