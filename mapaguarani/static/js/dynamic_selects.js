document.addEventListener("DOMContentLoaded", function () {
    const countrySelect = document.getElementById("id_country");
    const stateSelect = document.getElementById("id_states");
    const citySelect = document.getElementById("id_cities");

    // Função para limpar todas as opções de um select e adicionar o placeholder
    function clearSelect(selectElement, placeholderText) {
        selectElement.innerHTML = `<option value="">${placeholderText}</option>`;
    }

    // Função para remover o placeholder (value="") apenas se nenhum item estiver selecionado
    function clearPlaceholderIfNoSelection(selectElement) {
        const placeholderOption = selectElement.querySelector('option[value=""]');
        const hasSelection = Array.from(selectElement.options).some(option => option.selected && option.value !== "");
        if (placeholderOption && !hasSelection) {
            selectElement.removeChild(placeholderOption);
        }
    }

    // Função para obter os valores selecionados em um select múltiplo
    function getSelectedValues(selectElement) {
        return Array.from(selectElement.options)
            .filter(option => option.selected && option.value !== "")
            .map(option => option.value);
    }
    // Executa o clearPlaceholderIfNoSelection nos selects de estado e         
    if (typeof countrySelect !== "undefined" && countrySelect !== null && countrySelect.value === "") {         clearPlaceholderIfNoSelection(stateSelect);
        clearPlaceholderIfNoSelection(citySelect);
        clearSelect(stateSelect, 'Selecione um país');
        clearSelect(citySelect, 'Selecione uma país');
    }
    else{

        if (typeof stateSelect !== "undefined" && stateSelect !== null && stateSelect.value === "") {            clearPlaceholderIfNoSelection(stateSelect);
            clearSelect(stateSelect, 'Selecione um estado');
        }
        if (typeof citySelect !== "undefined" && citySelect !== null && citySelect.value === "") {            clearPlaceholderIfNoSelection(citySelect);
            clearSelect(citySelect, 'Selecione uma cidade');
        }
    }
    
    if (typeof countrySelect !== "undefined" && countrySelect !== null) {
        // Evento de mudança para o select de país
        countrySelect.addEventListener("change", function () {
            const countryId = this.value;

            clearSelect(citySelect, 'Selecione uma cidade');

            if (countryId) {
                clearSelect(stateSelect, 'Carregando');
                fetch(`/get_states/${countryId}/`)
                    .then(response => response.json())
                    .then(states => {
                        states.forEach(state => {
                            const option = document.createElement("option");
                            option.value = state.id;
                            option.textContent = state.name;
                            stateSelect.appendChild(option);
                        });
                        clearPlaceholderIfNoSelection(stateSelect);
                    })
                    .catch(error => console.error('Erro ao buscar estados:', error));
            } else {
                clearSelect(stateSelect, 'Selecione um estado');
            }
        });
    }
    if (typeof stateSelect !== "undefined" && stateSelect !== null) {
    // Evento de mudança para o select de estado
        stateSelect.addEventListener("change", function () {
            const selectedStateIds = getSelectedValues(stateSelect);

            if (selectedStateIds.length > 0) {
                clearSelect(citySelect, 'Carregando');
                fetch(`/get_cities/?state_ids=${selectedStateIds.join(',')}`)
                    .then(response => response.json())
                    .then(cities => {
                        cities.forEach(city => {
                            const option = document.createElement("option");
                            option.value = city.id;
                            option.textContent = city.name;
                            citySelect.appendChild(option);
                        });
                        clearPlaceholderIfNoSelection(citySelect);
                    })
                    .catch(error => console.error('Erro ao buscar cidades:', error));
            } else {
                clearSelect(citySelect, 'Selecione uma cidade');
            }
        });
    }
});
